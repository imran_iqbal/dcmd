use std::fs::File;
use std::io::Read;
use std::path::Path;
use std::path::PathBuf;
use std::process::Command;
use std::sync::{mpsc, Arc, RwLock};
use std::thread;
use std::time::{Duration, SystemTime};

use clap::Parser;
use notify::{RecommendedWatcher, RecursiveMode, Watcher};
use serde::Deserialize;

#[derive(Parser, Debug)]
#[clap(name = "dcmd")]
struct Cli {
    /// Path to directory to watch
    directory: String,
    /// Path to run commands in, if not specified directory is used
    #[clap(short, long)]
    command_directory: Option<String>,
    /// Amount of time to wait before executing commands (in ms)
    #[clap(short, long)]
    wait: Option<u64>,
    /// Required. Path to config file
    #[clap(long)]
    conf: String,
}

#[derive(Debug)]
struct Tick {
    time: SystemTime,
    from_fs: bool,
}

struct RunConfig {
    wait: u64,
    watch_dir: PathBuf,
    base_dir: PathBuf,
    commands: Vec<UserCommand>,
}

impl RunConfig {
    fn run_commands(&self) {
        for c in &self.commands {
            let mut cd = self.base_dir.clone();

            if let Some(d) = &c.directory {
                cd = cd.join(d);
            }

            let res = match &c.args {
                Some(args) => Command::new(&c.command)
                    .args(args)
                    .current_dir(cd)
                    .status()
                    .expect("failed to run process"),
                None => Command::new(&c.command)
                    .current_dir(cd)
                    .status()
                    .expect("failed to run process"),
            };

            if !res.success() && !c.continue_on_failure {
                println!("Failed to run {:?}", c);
                return;
            }
        }
    }
}

#[derive(Deserialize)]
struct Config {
    #[serde(default)]
    wait: Option<u64>,
    #[serde(default)]
    base_command_dir: Option<String>,
    commands: Vec<UserCommand>,
}

impl Config {
    fn merge(self, cli_args: Cli) -> RunConfig {
        let mut wait: u64 = 2000;

        if let Some(w) = self.wait {
            wait = w;
        }

        if let Some(w) = cli_args.wait {
            wait = w;
        }

        let watch_dir: PathBuf = cli_args.directory.into();

        let mut base_dir = watch_dir.clone();

        if let Some(b) = self.base_command_dir {
            base_dir = PathBuf::from(b);
        }

        if let Some(b) = cli_args.command_directory {
            base_dir = PathBuf::from(b);
        }

        RunConfig {
            wait,
            commands: self.commands,
            watch_dir,
            base_dir,
        }
    }
}

#[derive(Deserialize, Debug)]
struct UserCommand {
    command: String,
    #[serde(default)]
    args: Option<Vec<String>>,
    directory: Option<String>,
    #[serde(default)]
    continue_on_failure: bool,
}

fn timer_thread(last_tick: Arc<RwLock<Tick>>, rc: RunConfig) {
    loop {
        thread::sleep(Duration::from_millis(250));

        let read_tick = last_tick.read().unwrap();

        let elapsed_time = read_tick.time.elapsed().unwrap();

        if !read_tick.from_fs || elapsed_time.as_millis() < rc.wait as u128 {
            continue;
        }

        drop(read_tick);
        rc.run_commands();
        let mut write_lock = last_tick.write().unwrap();
        write_lock.time = SystemTime::now();
        write_lock.from_fs = false;
    }
}

fn set_tick(last_tick: Arc<RwLock<Tick>>) {
    let mut write_lock = last_tick.write().unwrap();
    write_lock.time = SystemTime::now();
    write_lock.from_fs = true;
}

fn create_watcher(last_tick: Arc<RwLock<Tick>>, directory: &Path) {
    let (tx, rx) = mpsc::channel();

    let mut watcher =
        RecommendedWatcher::new(tx, notify::Config::default()).expect("Watcher to be created");

    watcher
        .watch(directory, RecursiveMode::Recursive)
        .expect("There was an issue starting directory watcher");

    for res in rx {
        match res {
            Ok(_) => set_tick(last_tick.clone()),
            Err(e) => println!("Watch error : {:?}", e),
        }
    }
}

fn main() {
    let options = Cli::parse();

    let conf_file = File::open(&options.conf);
    if let Err(err) = conf_file {
        println!("Failed to open conf file: {:?}", err);
        std::process::exit(1);
    }

    let mut conf_content = String::new();
    conf_file
        .unwrap()
        .read_to_string(&mut conf_content)
        .expect("to read conf file");

    let conf: Config = toml::from_str(&conf_content).expect("valid config file");

    let run_conf = conf.merge(options);

    let last_tick = Arc::new(RwLock::new(Tick {
        time: SystemTime::now(),
        from_fs: false,
    }));

    let watch_dir = run_conf.watch_dir.clone();
    let thread_tick = last_tick.clone();
    thread::Builder::new()
        .name("Timer thread".into())
        .spawn(move || timer_thread(thread_tick, run_conf))
        .unwrap();

    create_watcher(last_tick, &watch_dir);
}
